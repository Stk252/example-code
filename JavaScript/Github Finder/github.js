// For optimal results, register the app at: https://github.com/settings/applications/new
class Github {
	constructor() {
		// Not to do on production environment
		this.client_id = 'a7405ddeabf491e09c63';
		this.client_secret = 'b649b5dfc55b31ad408f8dbbf6586c28f1fcc529';
		this.repos_count = 5;
		this.repos_sort = 'created: asc';
	}

	async getUser(user) {
		const profileResponse = await fetch(`https://api.github.com/users/${user}?client_id=${this.client_id}&client_secret=${this.client_secret}`);

		const repoResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.repos_count}&sort=${this.repos_sort}&client_id=${this.client_id}&client_secret=${this.client_secret}`);

		const profile = await profileResponse.json();
		const repos = await repoResponse.json();

		return {
			profile,
			repos
		}
	}
}