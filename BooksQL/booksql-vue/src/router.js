import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Book from "./views/Book";
import AddBook from "./views/AddBook";
import EditBook from "./views/EditBook";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/books/add',
      name: 'addBook',
      component: AddBook
    },
    {
      path: '/books/:id',
      name: 'book',
      component: Book
    },
    {
      path: '/books/:id/edit',
      name: 'editBook',
      component: EditBook
    },
  ]
})
