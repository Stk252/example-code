<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      itemOperations={
 *          "get"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object == user",
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-user-details"
 *                  }
 *              }
 *          },
 *          "put"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object == user",
 *              "denormalization_context"={
 *                  "groups"={
 *                      "put"
 *                  }
 *              },
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-user-details"
 *                  }
 *              }
 *          },
 *          "delete"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object == user"
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-users-list"
 *                  }
 *              }
 *          },
 *          "post"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')",
 *              "denormalization_context"={
 *                  "groups"={
 *                      "post"
 *                  }
 *              },
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-users-list"
 *                  }
 *              }
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User implements UserInterface, CreationDateEntityInterface
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_MANAGER = 'ROLE_MANAGER';
    const ROLE_DEV = 'ROLE_DEV';

    const DEFAULT_ROLES = [self::ROLE_DEV];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-user-details", "get-users-list", "get-project-details", "get-projects-list", "get-task-details", "get-tasks-list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-user-details", "get-users-list", "post", "get-project-details", "get-projects-list", "get-task-details", "get-tasks-list"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,}/",
     *     message="Password must be at least 7 characters long and contain at least 1 digit, 1 uppercase letter and 1 lowercase letter."
     * )
     * @Groups({"put", "post"})
     */
    private $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Expression(
     *     "this.getPassword() === this.getConfirmationPassword()",
     *     message="The passwords do not match."
     * )
     * @Groups({"put", "post"})
     */
    private $confirmationPassword;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-user-details", "get-users-list", "put", "post", "get-project-details", "get-projects-list", "get-task-details", "get-tasks-list"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Groups({"get-user-details", "get-users-list", "put", "post", "get-project-details", "get-projects-list", "get-task-details", "get-tasks-list"})
     */
    private $email;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     * @Groups({"get-user-details", "put", "post"})
     */
    private $birthDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get-user-details"})
     */
    private $creationDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="publisher", cascade={"persist"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="assignee", cascade={"persist"})
     * @Groups({"get-user-details"})
     */
    private $assignedTasks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="publisher", cascade={"persist"})
     * @Groups({"get-user-details"})
     */
    private $projects;

    /**
     * @ORM\Column(type="simple_array", length=200)
     * @Groups({"get-user-details", "get-users-list", "put", "post", "get-project-details", "get-projects-list", "get-task-details", "get-tasks-list"})
     */
    private $roles;

    public function __construct()
    {
        $this->assignedTasks = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->roles = self::DEFAULT_ROLES;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): CreationDateEntityInterface
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @param Collection $comments
     * @return self
     */
    public function setComments($comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAssignedTasks(): Collection
    {
        return $this->assignedTasks;
    }

    /**
     * @param $assignedTasks
     * @return self
     */
    public function setAssignedTasks($assignedTasks): self
    {
        $this->assignedTasks = $assignedTasks;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @param $projects
     * @return self
     */
    public function setProjects($projects): self
    {
        $this->projects = $projects;

        return $this;
    }



    public function getConfirmationPassword()
    {
        return $this->confirmationPassword;
    }

    public function setConfirmationPassword($confirmationPassword): void
    {
        $this->confirmationPassword = $confirmationPassword;
    }


    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {

    }
}
