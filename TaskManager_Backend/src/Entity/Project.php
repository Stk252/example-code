<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-project-details"
 *                  }
 *              }
 *          },
 *          "put_as_manager"={
 *              "method"="PUT",
 *              "access_control"="is_granted('ROLE_MANAGER') and object.getPublisher() == user",
 *              "groups"={"update_project_as_publisher"}
 *          },
 *          "put_as_admin"={
 *              "method"="PUT",
 *              "access_control"="is_granted('ROLE_ADMIN')",
 *              "groups"={"update_project_as_admin"}
 *          },
 *          "delete"={
 *              "access_control"="is_granted('ROLE_ADMIN') or object.getPublisher() == user"
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-projects-list"
 *                  }
 *              }
 *          },
 *          "post"={
 *              "access_control"="is_granted('ROLE_MANAGER')"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @UniqueEntity("name")
 */
class Project implements CreationDateEntityInterface, PublishedEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-project-details", "get-projects-list", "get-user-details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-project-details", "get-projects-list", "get-user-details", "update_project_as_admin", "update_project_as_publisher"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Groups({"get-project-details", "get-projects-list", "update_project_as_admin", "update_project_as_publisher"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get-project-details"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-project-details", "get-projects-list", "update_project_as_admin"})
     */
    private $publisher;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="project", cascade={"remove"})
     * @Groups({"get-project-details"})
     */
    private $tasks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="project", cascade={"remove"})
     * @Groups({"get-project-details"})
     */
    private $comments;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): CreationDateEntityInterface
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    /**
     * @param $tasks
     *
     * @return self
     */
    public function setTasks($tasks): self
    {
        $this->tasks = $tasks;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getComments() : Collection
    {
        return $this->comments;
    }

    /**
     * @param Collection $comments
     * @return self
     */
    public function setComments(Collection $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return User
     */
    public function getPublisher(): User
    {
        return $this->publisher;
    }

    /**
     * @param UserInterface $manager
     * @return PublishedEntityInterface
     */
    public function setPublisher(UserInterface $manager): PublishedEntityInterface
    {
        $this->publisher = $manager;

        return $this;
    }
}
