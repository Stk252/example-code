<?php


namespace App\Entity;


interface CreationDateEntityInterface
{
    public function setCreationDate(\DateTimeInterface $creationDate): CreationDateEntityInterface;
}