<?php


namespace App\Entity;


use Symfony\Component\Security\Core\User\UserInterface;

interface PublishedEntityInterface
{
    public function setPublisher(UserInterface $user): PublishedEntityInterface;
}