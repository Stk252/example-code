<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-task-details"
 *                  }
 *              }
 *
 *          },
 *          "put"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *          },
 *          "delete"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={
 *                      "get-tasks-list"
 *                  }
 *              }
 *          },
 *          "post"={
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @UniqueEntity("name")
 */
class Task implements CreationDateEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-task-details", "get-tasks-list", "get-project-details", "get-user-details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-task-details", "get-tasks-list", "get-project-details", "get-user-details"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Groups({"get-task-details"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-task-details", "get-tasks-list", "get-project-details", "get-user-details"})
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Groups({"get-task-details", "get-tasks-list", "get-project-details", "get-user-details"})
     */
    private $percentComplete;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-task-details"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-task-details"})
     */
    private $severity;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"get-task-details"})
     */
    private $priority;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get-task-details"})
     */
    private $creationDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     * @Groups({"get-task-details"})
     */
    private $estimatedCompletionDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     * @Groups({"get-task-details"})
     */
    private $realCompletionDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="tasks", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-task-details", "get-tasks-list", "get-user-details"})
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="assignedTasks", cascade={"persist"})
     * @Groups({"get-task-details", "get-tasks-list", "get-project-details"})
     */
    private $assignee;


    public function __construct()
    {
        $this->assignee = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPercentComplete(): ?int
    {
        return $this->percentComplete;
    }

    public function setPercentComplete(int $percentComplete): self
    {
        $this->percentComplete = $percentComplete;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSeverity(): ?string
    {
        return $this->severity;
    }

    public function setSeverity(string $severity): self
    {
        $this->severity = $severity;

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): CreationDateEntityInterface
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getEstimatedCompletionDate(): ?\DateTimeInterface
    {
        return $this->estimatedCompletionDate;
    }

    public function setEstimatedCompletionDate(?\DateTimeInterface $estimatedCompletionDate): self
    {
        $this->estimatedCompletionDate = $estimatedCompletionDate;

        return $this;
    }

    public function getRealCompletionDate(): ?\DateTimeInterface
    {
        return $this->realCompletionDate;
    }

    public function setRealCompletionDate(?\DateTimeInterface $realCompletionDate): self
    {
        $this->realCompletionDate = $realCompletionDate;

        return $this;
    }



    /**
     *
     */
    public function getAssignee()
    {
        return $this->assignee;
    }

    /**
     * @param $assignee
     * @return self
     */
    public function setAssignee($assignee): self
    {
        $this->assignee = $assignee;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return self
     */
    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }


}
