<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordHashSubscriber implements EventSubscriberInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    // We need the symfony password encoder to be injected once we call the PasswordHashSubscriber once in order
    // to hash the password every time it's called
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        // Will watch the kernel events, and execute a function 'hashPassword' before writing the User to
        // The database
        return [
            KernelEvents::VIEW => ['hashPassword', EventPriorities::PRE_WRITE]
        ];
    }

    // Method called after and event is triggered, will get the event, check if it's a user and that the
    // Expected method is used, and finally perform an action, in this case, encoding a password
    public function hashPassword(GetResponseForControllerResultEvent $event) {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        // Check if it's a User and that the method was Post
        if (! $user instanceof User || !in_array($method, [Request::METHOD_POST, Request::METHOD_PUT])) {
            return;
        }

        // If all checks pass, hash the password using symfony's password encoder which we initialized in the constructor
        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

    }
}