<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private const STATUSES = ['ToDo', 'Doing', 'Done'];

    private const PERCENTS = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

    private const TYPES = ['Bug', 'Feature Request'];

    private const SEVERITY = ['Very Low', 'Low', 'Medium', 'High', 'Very High', 'Critical'];

    private const PRIORITIES = ['Very Low', 'Low', 'Medium', 'High', 'Very High'];

    private const USERS = [
        [
            'username' => 'walt',
            'password' => 'Password123',
            'name' => 'Walt Disney',
            'email' => 'walt@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_ADMIN]
        ],
        [
            'username' => 'mickey',
            'password' => 'Password123',
            'name' => 'Mickey Mouse',
            'email' => 'mickey@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'minnie',
            'password' => 'Password123',
            'name' => 'Minnie Mouse',
            'email' => 'minnie@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'donald',
            'password' => 'Password123',
            'name' => 'Donald Duck',
            'email' => 'donald@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'goofy',
            'password' => 'Password123',
            'name' => 'Goofy Goof',
            'email' => 'goofy@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'max',
            'password' => 'Password123',
            'name' => 'Max Goof',
            'email' => 'max@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'scrooge',
            'password' => 'Password123',
            'name' => 'Scrooge McDuck',
            'email' => 'scrooge@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'pete',
            'password' => 'Password123',
            'name' => 'Peter Pete Sr.',
            'email' => 'pete@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'huey',
            'password' => 'Password123',
            'name' => 'Huey Duck',
            'email' => 'huey@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'Dewey',
            'password' => 'Password123',
            'name' => 'Dewey Duck',
            'email' => 'dewey@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'louie',
            'password' => 'Password123',
            'name' => 'Louie Duck',
            'email' => 'louie@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'clarabelle',
            'password' => 'Password123',
            'name' => 'Clarabelle Cow',
            'email' => 'clarabelle@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'mortimer',
            'password' => 'Password123',
            'name' => 'Mortimer Mouse',
            'email' => 'mortimer@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'simba',
            'password' => 'Password123',
            'name' => 'Simba',
            'email' => 'simba@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],
        [
            'username' => 'mufasa',
            'password' => 'Password123',
            'name' => 'Mufasa',
            'email' => 'mufasa@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'scar',
            'password' => 'Password123',
            'name' => 'Scar',
            'email' => 'scar@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'pumba',
            'password' => 'Password123',
            'name' => 'Pumba',
            'email' => 'pumba@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'timon',
            'password' => 'Password123',
            'name' => 'Timon',
            'email' => 'timon@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'nala',
            'password' => 'Password123',
            'name' => 'Nala',
            'email' => 'nala@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'jaq',
            'password' => 'Password123',
            'name' => 'Jaq',
            'email' => 'jaq@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'gus',
            'password' => 'Password123',
            'name' => 'Gus',
            'email' => 'gus@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'lucifer',
            'password' => 'Password123',
            'name' => 'Lucifer',
            'email' => 'lucifer@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'winnie',
            'password' => 'Password123',
            'name' => 'Winnie the Pooh',
            'email' => 'winnie@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'tigger',
            'password' => 'Password123',
            'name' => 'Tigger',
            'email' => 'tigger@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'piglet',
            'password' => 'Password123',
            'name' => 'Piglet',
            'email' => 'piglet@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'roo',
            'password' => 'Password123',
            'name' => 'Roo',
            'email' => 'roo@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'eeyore',
            'password' => 'Password123',
            'name' => 'Eeyore',
            'email' => 'eeyore@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'kanga',
            'password' => 'Password123',
            'name' => 'Kanga',
            'email' => 'kanga@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'zazu',
            'password' => 'Password123',
            'name' => 'Zazu',
            'email' => 'zazu@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'rafiki',
            'password' => 'Password123',
            'name' => 'Rafiki',
            'email' => 'rafiki@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'lumiere',
            'password' => 'Password123',
            'name' => 'Lumière',
            'email' => 'lumiere@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'cogsworth',
            'password' => 'Password123',
            'name' => 'Cogsworth',
            'email' => 'cogsworth@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'potts',
            'password' => 'Password123',
            'name' => 'Mrs. Potts',
            'email' => 'potts@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ],[
            'username' => 'chip',
            'password' => 'Password123',
            'name' => 'Chip Potts',
            'email' => 'chip@blog.com',
            'birth_date' => '1991-05-01',
            'creation_date' => '2018-07-02 12:00:00',
            'roles' => [User::ROLE_DEV]
        ]
    ];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var \Faker\Factory
     */
    private $faker;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = \Faker\Factory::create();
    }

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadProjects($manager);
        $this->loadTasks($manager);
        $this->loadComments($manager);
    }

    public function loadProjects(ObjectManager $manager)
    {
        for ($i=0; $i < 20; $i++) {
            $project = new Project();
            $project->setName($this->faker->realText(30));
            $project->setDescription($this->faker->realText(200));
            $project->setCreationDate($this->faker->dateTimeThisYear);
            $project->setPublisher($this->getRandomManagerReference());


            // Uncomment if decide to add members to projects again
//            for ($j = 0; $j < rand(0, 6); $j++) {
//                $user = $this->getRandomUserReference();
//                if (!$project->getMembers()->contains($user)) {
//                    $project->getMembers()->add($user);
//                }
//            }

            $this->setReference("project_$i", $project);

            $manager->persist($project);
        }
        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager) {
        foreach (self::USERS as $userFixture) {
            $user = new User();
            $user->setUsername($userFixture['username']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $userFixture['password']
            ));
            $user->setName($userFixture['name']);
            $user->setEmail($userFixture['email']);
            $user->setBirthDate(new \DateTime($userFixture['birth_date']));
            $user->setCreationDate(new \DateTime($userFixture['creation_date']));
            $user->setRoles($userFixture['roles']);

            // Adding reference allows us to use the generated entity into another entity that requires it
            $this->addReference('user_' . $userFixture['username'], $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function loadTasks(ObjectManager $manager) {
        for ($i=0; $i < 20; $i++) {
            for ($j = 0; $j < rand(0, 15); $j++) {
                $task = new Task();
                $task->setName($this->faker->realText(30));
                $task->setDescription($this->faker->realText());
                $task->setStatus(self::STATUSES[rand(0, 2)]);
                $task->setPercentComplete(self::PERCENTS[rand(0, 9)]);
                $task->setType(self::TYPES[rand(0, 1)]);
                $task->setSeverity(self::SEVERITY[rand(0, 5)]);
                $task->setPriority(self::PRIORITIES[rand(0, 4)]);
                $task->setCreationDate($this->faker->dateTimeThisYear);
                $task->setEstimatedCompletionDate($this->faker->dateTimeThisYear);
                $task->setProject($this->getReference("project_$i"));
                $task->setAssignee($this->getRandomUserReference());

                $this->setReference("task_$i", $task);

                $manager->persist($task);
            }
        }


        $manager->flush();
    }

    public function loadComments(ObjectManager $manager) {
        for ($i = 0; $i < 20; $i++) {
            for ($j = 0; $j < rand(0, 10); $j++) {
                $comment = new Comment();
                $comment->setContent($this->faker->realText());
                $comment->setCreationDate($this->faker->dateTimeThisYear);

                $authorReference = $this->getRandomUserReference();

                $comment->setPublisher($authorReference);
                $comment->setProject($this->getReference("project_$i"));

                $manager->persist($comment);
            }
        }

        $manager->flush();
    }

    protected function getRandomUserReference(): User
    {
        return $this->getReference('user_' . self::USERS[rand(0, 33)]['username']);
    }

    protected function getRandomManagerReference(): User
    {
        return $this->getReference('user_' . self::USERS[rand(0, 12)]['username']);
    }
}
