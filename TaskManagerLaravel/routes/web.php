<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


// Routes accessible only to admins and managers
Route::middleware(['auth', 'manager'])->group(function () {
    // Project Routes
    Route::get('projects/create', 'ProjectController@create')->name('projects.create');
    Route::post('projects', 'ProjectController@store')->name('projects.store');
    Route::put('projects/{project}', 'ProjectController@update')->name('projects.update');
    Route::delete('projects/{project}', 'ProjectController@destroy')->name('projects.destroy');
    Route::get('projects/{project}/edit', 'ProjectController@edit')->name('projects.edit');

    Route::get('users', 'UserController@index')->name('users.index');
});




// Routes accessible to all authenticated users
Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    // User route seems not to work if put in the user routes section
    Route::get('users/profile', 'UserController@editProfile')->name('users.edit-profile');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::put('users/{user}', 'UserController@update')->name('users.update');
    Route::get('users/notifications', 'UserController@notifications')->name('users.notifications');

    Route::get('projects', 'ProjectController@index')->name('projects.index');
    Route::get('projects/{project}', 'ProjectController@show')->name('projects.show');

    // Task Routes
    Route::get('tasks', 'TaskController@index')->name('tasks.index');
    Route::post('tasks', 'TaskController@store')->name('tasks.store');
    Route::get('tasks/create', 'TaskController@create')->name('tasks.create');
    Route::delete('tasks/{task}', 'TaskController@destroy')->name('tasks.destroy');
    Route::get('tasks/{task}', 'TaskController@show')->name('tasks.show');
    Route::put('tasks/{task}', 'TaskController@update')->name('tasks.update');
    Route::get('tasks/{task}/edit', 'TaskController@edit')->name('tasks.edit');

    // Tag Routes
    Route::resource('tags', 'TagController');
    Route::resource('comments', 'CommentController');

    // User Routes
    Route::get('users/{user}', 'UserController@show')->name('users.show');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
});

// Routes accessible only to the admins
//Route::middleware(['auth', 'admin'])->group(function () {
//});
//
//



