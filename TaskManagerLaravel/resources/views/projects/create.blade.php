@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('air-datepicker/dist/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('select2/dist/css/select2.css') }}">
    <script type="text/javascript" src="{{ URL::asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
@endsection

@section('content')

    <div class="card card-default">
        <div class="card-header">
            {{ isset($project) ? 'Edit Project' : 'Create Project' }}
        </div>
        <div class="card-body">
            @include('partials.errors')
            <form id="project-form" action="{{ isset($project) ? route('projects.update', $project->id) : route('projects.store') }}" method="POST">
                @csrf
                @if (isset($project))
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ isset($project) ? $project->name : '' }}">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" >
                        @if (isset($project))
                            {{ $project->description }}
                        @endif
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select id="status" class="form-control" name="status">
                        <option @if (isset($project)) {{ $project->status == 'New' ? 'selected' : '' }} @endif>New</option>
                        <option @if (isset($project)) {{ $project->status == 'In Progress' ? 'selected' : '' }} @endif>In Progress</option>
                        <option @if (isset($project)) {{ $project->status == 'Finished' ? 'selected' : '' }} @endif>Finished</option>
                        <option @if (isset($project)) {{ $project->status == 'Closed' ? 'selected' : '' }} @endif>Closed</option>
                    </select>
                </div>

                @if (auth()->user()->isAdmin())
                    <div class="form-group">
                        <label for="manager">Manager</label>
                        <select id="manager" class="form-control" name="manager">
                            <option value="0">Select a manager</option>
                            @foreach($users as $user)
                                @if($user->isManager() || $user->isAdmin())
                                    <option value="{{ $user->id }}"
                                    @if (isset($project) && $project->manager == $user->id)
                                        selected
                                    @endif
                                    >
                                        {{ $user->name }}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                @endif

                @if($users->count() > 0)
                    <div class="form-group">
                        <label for="users">Members</label>
                        <select name="users[]" id="users" class="form-control" multiple>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}"
                                        @if (isset($project) && $project->hasUser($user->id))
                                        selected
                                        @endif
                                >
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                @endif

                <div class="form-group">
                    <label for="estimated_project_end">Estimated project end</label>
                    <input id="estimated_project_end" type="text" class="form-control" name="estimated_project_end" >
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">{{ isset($project) ? 'Update Project' : 'Add Project' }}</button>
                    <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('air-datepicker/dist/js/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('select2/dist/js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('select2/dist/js/i18n/en.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

{{--        Select2 Config  --}}
    <script>
        $(document).ready(function() {
            $('#users').select2();
        });
    </script>

    {{--    Datepicker config   --}}
    <script type="text/javascript">
        $('#estimated_project_end').datepicker({
            language: 'en',
            dateFormat: 'dd/mm/yyyy',
            firstDay: 1,
            position: 'top left',
            autoClose: true
        })
        @if (isset($project) && $project->estimated_project_end !== null)
            .data('datepicker').selectDate(new Date(
            parseInt('{{ substr($project->estimated_project_end, 0, 4) }}'),
            parseInt('{{ substr($project->estimated_project_end, 5, 7) }}') - 1,
            parseInt('{{ substr($project->estimated_project_end, 8, 10) }}'
            )));
        @endif
    </script>


    {{--    TinyMCE Config  --}}
    <script type="text/javascript">
        tinymce.init({
            selector: '#description',
            branding: false,
            plugins: 'autoresize link image imagetools table spellchecker lists hr emoticons insertdatetime table image media codesample code',
            toolbar: 'undo redo | ' +
                'formatselect fontselect fontsizeselect forecolor backcolor blockquote | ' +
                'hr | ' +
                'bold italic underline strikethrough subscript superscript | ' +
                'alignleft aligncenter alignright alignjustify | ' +
                'outdent indent | ' +
                'bullist numlist | ' +
                'insertdatetime emoticons | ' +
                'table image media | ' +
                'codesample code',
            autoresize_bottom_margin: 0,
            codesample_languages: [
                {text: 'HTML/XML', value: 'markup'},
                {text: 'JavaScript', value: 'javascript'},
                {text: 'CSS', value: 'css'},
                {text: 'PHP', value: 'php'},
                {text: 'Ruby', value: 'ruby'},
                {text: 'Python', value: 'python'},
                {text: 'Java', value: 'java'},
                {text: 'C', value: 'c'},
                {text: 'C#', value: 'csharp'},
                {text: 'C++', value: 'cpp'}
            ]
        });
    </script>
@endsection