@extends('layouts.app')


@section('content')


    <div class="card card-default">
        <div class="card-header d-flex justify-content-between">
            <h3>Projects</h3>
            @if(auth()->user()->isAdmin() || auth()->user()->isManager())
                <div>
                    <a href="{{ route('projects.create') }}" class="btn btn-success">Add Project</a>
                </div>
            @endif
        </div>
        <div class="card-body">

            @if ($projects->count() > 0)
            <h3>Search for a project</h3>
            <sup>Name or Status</sup>
            <form class="input-group" action="{{ route('projects.index') }}" method="GET">
                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->query('search') }}">
            </form>
            <hr>
                <div class="d-flex justify-content-center">
                    {{ $projects->appends(['search' => request()->query('search')])->links() }}
                </div>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Tasks</th>
                        <th>Members</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{ $project->name }}</td>
                            <td>{{ $project->status }}</td>
                            <td>{{ $project->tasks->count() }}</td>
                            <td>{{ $project->users->count() }}</td>
                            <td>
                                <a href="{{ route('projects.show', $project->id) }}" class="btn btn-primary btn-sm">View Project</a>
                                <button class="btn btn-danger btn-sm" onclick="handleDelete({{ $project->id }}, '{{ $project->name }}')">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                <div class="d-flex justify-content-center">
                    {{ $projects->appends(['search' => request()->query('search')])->links() }}
                </div>
            @else
                <h3 class="text-center">No projects yet</h3>
            @endif

            <!-- Modal -->
            <div class="modal fade" id="delete-project-modal" tabindex="-1" role="dialog" aria-labelledby="delete-project-modal-label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form id="delete-project-form" action="" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-project-modal-title"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-center text-bold">
                                    Are you sure you want to delete this project?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Confirm</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function handleDelete(id, name) {
            let form = document.querySelector('#delete-project-form');
            let title = document.querySelector('#delete-project-modal-title');

            title.textContent = `Delete Project: ${name}`;

            form.action = `projects/${id}`;
            $('#delete-project-modal').modal('show');
        }
    </script>

@endsection