@extends('layouts.app')

@section('content')


    <div class="card card-default">
        <div class="card-header d-flex justify-content-between">
            <h1>Project: {{ $project->name }}</h1>
            <div>
                <a href="{{ route('projects.edit', $project) }}" class="btn btn-success">Edit Project</a>
                <a href="{{ route('projects.index') }}" class="btn btn-primary">Projects Index</a>
            </div>
        </div>
        <div class="card-body">
            <h3>Information</h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Estimated completion date</th>
                    <th>Manager</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->status }}</td>
                    <td>{{ date('d/m/Y', strtotime($project->estimated_project_end)) }}</td>
                    <td>
                        @foreach($users as $user)
                            @if($project->manager != null && $user->id == $project->manager)
                                {{ $user->name }}
                            @endif
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
            <h3>Members</h3>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
                </thead>
                <tbody>
                @if($project->users->count() > 0)
                    @foreach ($users as $user)
                        @if ($project->hasUser($user->id))
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
            <br>
            <h3>Description</h3>
            <div class="card">
                <div id="description"
                     class="card-body">{!! html_entity_decode($project->description, ENT_QUOTES, 'utf-8') !!}</div>
            </div>

            <br>

            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h3>Tasks</h3>
                    <div>
                        <a href="{{ route('tasks.create') }}" class="btn btn-success">Add Task</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Completed</th>
                            <th>Type</th>
                            <th>Severity</th>
                            <th>Priority</th>
                            <th>Deadline</th>
                            <th>Project</th>
                            <th>Users</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project->tasks as $task)
                            <tr>
                                <td>{{ $task->name }}</td>
                                <td>{{ $task->status }}</td>
                                <td>{{ $task->completion_percentage }} %</td>
                                <td>{{ $task->type }}</td>
                                <td>{{ $task->severity }}</td>
                                <td>{{ $task->priority }}</td>
                                <td>{{ $task->deadline == null ? null : date('d/m/Y', strtotime($task->deadline))  }}</td>
                                <td>{{ $task->project->name }}</td>
                                <td>{{ $task->users()->count() }}</td>

                                <td>
                                    <a href="{{ route('tasks.show', $task) }}" class="btn btn-primary btn-sm">View Task</a>
                                    <button class="btn btn-danger btn-sm"
                                            onclick="handleDelete({{ $task->id }}, '{{ $task->name }}')">Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="delete-task-modal" tabindex="-1" role="dialog" aria-labelledby="delete-task-modal-label"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="delete-task-form" action="" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="delete-task-modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center text-bold">
                            Are you sure you want to delete this task?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Confirm</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        function handleDelete(id, name) {
            let form = document.querySelector('#delete-task-form');
            let title = document.querySelector('#delete-task-modal-title');

            title.textContent = `Delete Task: ${name}`;

            form.action = `../tasks/${id}`;
            $('#delete-task-modal').modal('show');
        }
    </script>

@endsection