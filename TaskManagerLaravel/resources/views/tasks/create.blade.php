@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('air-datepicker/dist/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('select2/dist/css/select2.css') }}">
    <script type="text/javascript" src="{{ URL::asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
@endsection


@section('content')

    <div class="card card-default">
        <h3 class="card-header">
            {{ isset($task) ?  'Task: ' . $task->name  : 'Create Task' }}
        </h3>
        <div class="card-body">
            @include('partials.errors')
            <form action="{{ isset($task) ? route('tasks.update', $task->id) : route('tasks.store') }}" method="POST" id="task-form">
                @csrf
                @if (isset($task))
                    @method('PUT')
                @endif

                @if(isset($task))
                    <div class="progress mb-5">
                        <div class="progress-bar" role="progressbar" style="width: {{ $task->completion_percentage }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ $task->completion_percentage }}%</div>
                    </div>
                @endif

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ isset($task) ? $task->name : '' }}">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description">
                        {{ isset($task) ? $task->description : '' }}
                    </textarea>
                </div>

                @if(!isset($task))
                <div class="form-group">
                    <label for="project">Project</label>
                    <select name="project" id="project" class="form-control" onchange="loadProjectUsers()">
                        <option value="">Select a project</option>
                        @foreach($projects as $project)
                            <option value="{{ $project->id }}">{{ $project->name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif


                <div class="form-group">
                    <label for="users">Users</label>
                    <select name="users[]" id="users" class="form-control" multiple>
                        @if(isset($task))
                            @foreach($task->project->users as $user)
                                <option value="{{ $user->id }}"
                                    @if ($task->hasUser($user->id))
                                        selected
                                    @endif
                                >
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select id="status" class="form-control" name="status">
                        <option @if (isset($task)) {{ $task->status == 'On Hold' ? 'selected' : '' }} @endif>On Hold</option>
                        <option @if (isset($task)) {{ $task->status == 'In Progress' ? 'selected' : '' }} @endif>In Progress</option>
                        <option @if (isset($task)) {{ $task->status == 'Finished' ? 'selected' : '' }} @endif>Finished</option>
                        <option @if (isset($task)) {{ $task->status == 'Closed' ? 'selected' : '' }} @endif>Closed</option>
                    </select>
                </div>



                <div class="form-group">
                    <label for="completion_percentage">Completion Percentage</label>
                    <div class="input-group">
                        <select id="completion_percentage" class="form-control custom-select" name="completion_percentage">
                            <option @if (isset($task)) {{ $task->completion_percentage == 0 ? 'selected' : null }} @endif>0</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 10 ? 'selected' : null }} @endif>10</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 20 ? 'selected' : null }} @endif>20</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 30 ? 'selected' : null }} @endif>30</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 40 ? 'selected' : null }} @endif>40</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 50 ? 'selected' : null }} @endif>50</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 60 ? 'selected' : null }} @endif>60</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 70 ? 'selected' : null }} @endif>70</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 80 ? 'selected' : null }} @endif>80</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 90 ? 'selected' : null }} @endif>90</option>
                            <option @if (isset($task)) {{ $task->completion_percentage == 100 ? 'selected' : null }} @endif>100</option>
                        </select>
                        <div class="input-group-append">
                            <label class="input-group-text" for="completion_percentage">%</label>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="type">Type</label>
                    <select id="type" class="form-control" name="type">
                        <option @if (isset($task)) {{ $task->type == 'Bug' ? 'selected' : '' }} @endif>Bug</option>
                        <option @if (isset($task)) {{ $task->type == 'Feature' ? 'selected' : '' }} @endif>Feature</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="severity">Severity</label>
                    <select id="severity" class="form-control" name="severity">
                        <option @if (isset($task)) {{ $task->severity == 'Very Low' ? 'selected' : '' }} @endif>Very Low</option>
                        <option @if (isset($task)) {{ $task->severity == 'Low' ? 'selected' : '' }} @endif>Low</option>
                        <option @if (isset($task)) {{ $task->severity == 'Medium' ? 'selected' : '' }} @endif>Medium</option>
                        <option @if (isset($task)) {{ $task->severity == 'High' ? 'selected' : '' }} @endif>High</option>
                        <option @if (isset($task)) {{ $task->severity == 'Very High' ? 'selected' : '' }} @endif>Very High</option>
                        <option @if (isset($task)) {{ $task->severity == 'Critical' ? 'selected' : '' }} @endif>Critical</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="priority">Priority</label>
                    <select id="priority" class="form-control" name="priority">
                        <option @if (isset($task)) {{ $task->priority == 'Very Low' ? 'selected' : '' }} @endif>Very Low</option>
                        <option @if (isset($task)) {{ $task->priority == 'Low' ? 'selected' : '' }} @endif>Low</option>
                        <option @if (isset($task)) {{ $task->priority == 'Medium' ? 'selected' : '' }} @endif>Medium</option>
                        <option @if (isset($task)) {{ $task->priority == 'High' ? 'selected' : '' }} @endif>High</option>
                        <option @if (isset($task)) {{ $task->priority == 'Very High' ? 'selected' : '' }} @endif>Very High</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="deadline">Deadline</label>
                    <input type="text" class="form-control" name="deadline" id="deadline">
                </div>

                @if($tags->count() > 0)
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <select name="tags[]" id="tags" class="form-control" multiple>
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}"
                                        @if (isset($task) && $task->hasTag($tag->id))
                                        selected
                                        @endif
                                >
                                    {{ $tag->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-success">{{ isset($task) ? 'Update Task' : 'Add Task' }}</button>
                    <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('air-datepicker/dist/js/datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('select2/dist/js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('select2/dist/js/i18n/en.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('air-datepicker/dist/js/i18n/datepicker.en.js') }}"></script>

    {{--    Select2 Config  --}}
    <script>
        $(document).ready(function() {
            $('#tags').select2();
            $('#users').select2();
        });
    </script>

    {{--    Change users list depending on project  --}}
    @if(!isset($task))
    <script>
        // Initialize array of projects with their respective members
        let projectUsers = [
            @foreach($projects as $project)
            {
                projectId: '{{ $project->id }}',
                projectUsers: [
                    @foreach($project->users as $user)
                        {{ $loop->first ? '' : ', ' }}
                        {
                            id: '{{ $user->id }}',
                            name: '{{ $user->name }}'
                        }
                    @endforeach
                ]
            },
            @endforeach
        ];

        // Loading the users depending on the selected project
        function loadProjectUsers() {
            // Getting usersNode: users select input
            let usersNode = document.getElementById('users');
            // Getting the value of the selected project: id of the project
            let value = document.getElementById("project").value;

            // Removing all current options on the userNode
            while (usersNode.firstChild) {
                usersNode.removeChild(usersNode.firstChild);
            }

            // Adding a new option for each user on the selected project
            projectUsers.forEach(function (project) {
                if (project.projectId === value) {
                    project.projectUsers.forEach(function (user) {
                        // Adding an option to select2: provided by select2 documentation
                        let data = {
                            id: user.id,
                            text: user.name
                        };

                        let newOption = new Option(data.text, data.id, false, false);
                        $('#users').append(newOption).trigger('change');
                    });
                }
            });
        }
    </script>
    @endif



    {{--    Datepicker config   --}}
    <script type="text/javascript">
        $('#deadline').datepicker({
            language: 'en',
            dateFormat: 'dd/mm/yyyy',
            firstDay: 1,
            position: 'top left',
            autoClose: true
        })
        @if (isset($task) && $task->deadline !== null)
            .data('datepicker').selectDate(new Date(
            parseInt('{{ substr($task->deadline, 0, 4) }}'),
            parseInt('{{ substr($task->deadline, 5, 7) }}') - 1,
            parseInt('{{ substr($task->deadline, 8, 10) }}'
            )));
        @endif
    </script>

    {{--    TinyMCE Config  --}}
    <script type="text/javascript">
        tinymce.init({
            selector: '#description',
            branding: false,
            plugins: 'autoresize link image imagetools table spellchecker lists hr emoticons insertdatetime table image media codesample code',
            toolbar: 'undo redo | ' +
                'formatselect fontselect fontsizeselect forecolor backcolor blockquote | ' +
                'hr | ' +
                'bold italic underline strikethrough subscript superscript | ' +
                'alignleft aligncenter alignright alignjustify | ' +
                'outdent indent | ' +
                'bullist numlist | ' +
                'insertdatetime emoticons | ' +
                'table image media | ' +
                'codesample code',
            autoresize_bottom_margin: 0,
            codesample_languages: [
                {text: 'HTML/XML', value: 'markup'},
                {text: 'JavaScript', value: 'javascript'},
                {text: 'CSS', value: 'css'},
                {text: 'PHP', value: 'php'},
                {text: 'Ruby', value: 'ruby'},
                {text: 'Python', value: 'python'},
                {text: 'Java', value: 'java'},
                {text: 'C', value: 'c'},
                {text: 'C#', value: 'csharp'},
                {text: 'C++', value: 'cpp'}
            ]
        });
    </script>

@endsection