@extends('layouts.app')

@section('content')

    <div class="card card-default">
        <div class="card-header d-flex justify-content-between">
            <h1>Task: {{ $task->name }}</h1>
            <div>
                <a href="{{ route('tasks.edit', $task) }}" class="btn btn-success">Edit</a>
                <a href="{{ route('tasks.index') }}" class="btn btn-primary">Back to task index</a>
                <a href="{{ route('projects.show', $task->project_id) }}" class="btn btn-primary">Back to project</a>
            </div>
        </div>
        <div class="card-body">
            <div class="progress mb-5">
                <div class="progress-bar" role="progressbar" style="width: {{ $task->completion_percentage }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ $task->completion_percentage }}%</div>
            </div>

            <h3>Information</h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Project</th>
                    <th>Status</th>
                    <th>Completed</th>
                    <th>Type</th>
                    <th>Severity</th>
                    <th>Priority</th>
                    <th>Deadline</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->project->name }}</td>
                    <td>{{ $task->status }}</td>
                    <td>{{ $task->completion_percentage }}</td>
                    <td>{{ $task->type }}</td>
                    <td>{{ $task->severity }}</td>
                    <td>{{ $task->priority }}</td>
                    <td>{{ date('d/m/Y', strtotime($task->deadline)) }}</td>
                </tr>
                </tbody>
            </table>
            <br>
            <h3>Members</h3>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
                </thead>
                <tbody>
                @if($task->users->count() > 0)
                    @foreach ($users as $user)
                        @if ($task->hasUser($user->id))
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role }}</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
            <br>
            <h3>Description</h3>
            <div class="card">
                <div id="description"
                     class="card-body">{!! html_entity_decode($task->description, ENT_QUOTES, 'utf-8') !!}</div>
            </div>
        </div>
    </div>


@endsection