@extends('layouts.app')


@section('content')

    <div class="card card-default">
        <div class="card-header d-flex justify-content-between">
            <h3>Tasks</h3>
            <div>
                <a href="{{ route('tasks.create') }}" class="btn btn-success">Add Task</a>
            </div>
        </div>
        <div class="card-body">
            @if ($tasks->count() > 0)
            <h3>Search for a task</h3>
            <sup>Name, Status, Type, Severity or Priority</sup>
            <form class="input-group" action="{{ route('tasks.index') }}" method="GET">
                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->query('search') }}">
            </form>
            <hr>
                <div class="d-flex justify-content-center">
                    {{ $tasks->appends(['search' => request()->query('search')])->links() }}
                </div>
                <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Completed</th>
                    <th>Type</th>
                    <th>Severity</th>
                    <th>Priority</th>
                    <th>Deadline</th>
                    <th>Project</th>
                    <th>Users</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{ $task->name }}</td>
                        <td>{{ $task->status }}</td>
                        <td>{{ $task->completion_percentage }} %</td>
                        <td>{{ $task->type }}</td>
                        <td>{{ $task->severity }}</td>
                        <td>{{ $task->priority }}</td>
                        <td>{{ $task->deadline == null ? null : date('d/m/Y', strtotime($task->deadline))  }}</td>
                        <td>{{ $task->project->name }}</td>
                        <td>{{ $task->users()->count() }}</td>

                        <td>
                            <a href="{{ route('tasks.show', $task) }}" class="btn btn-primary btn-sm">View Task</a>
                            <button class="btn btn-danger btn-sm" onclick="handleDelete({{ $task->id }}, '{{ $task->name }}')">Delete</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
                <div class="d-flex justify-content-center">
                    {{ $tasks->appends(['search' => request()->query('search')])->links() }}
                </div>
            @else
                <h3 class="text-center">No tasks yet</h3>
            @endif


            <!-- Modal -->
            <div class="modal fade" id="delete-task-modal" tabindex="-1" role="dialog" aria-labelledby="delete-task-modal-label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form id="delete-task-form" action="" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-task-modal-title"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-center text-bold">
                                    Are you sure you want to delete this task?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Confirm</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

    <script>
        function handleDelete(id, name) {
            let form = document.querySelector('#delete-task-form');
            let title = document.querySelector('#delete-task-modal-title');

            title.textContent = `Delete Task: ${name}`;

            form.action = `tasks/${id}`;
            $('#delete-task-modal').modal('show');
        }
    </script>

@endsection