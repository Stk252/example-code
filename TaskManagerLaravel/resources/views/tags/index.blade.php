@extends('layouts.app')


@section('content')
    <div class="card card-default">
        <div class="card-header d-flex justify-content-between">
            <h3>Tags</h3>
            <div>
                <a href="{{ route('tags.create') }}" class="btn btn-success">Add Tag</a>
            </div>
        </div>
        <div class="card-body">
            @if ($tags->count() > 0)
            <h3>Search for a Tag</h3>
            <sup>Name</sup>
            <form class="input-group" action="{{ route('tags.index') }}" method="GET">
                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->query('search') }}">
            </form>
            <hr>
                <div class="d-flex justify-content-center">
                    {{ $tags->appends(['search' => request()->query('search')])->links() }}
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Tasks</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tags as $tag)
                        <tr>
                            <td>{{ $tag->name }}</td>
                            <td>
                                {{ $tag->tasks->count() }}
                            </td>
                            <td>
                                <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-success btn-sm">Edit</a>
                                <button class="btn btn-danger btn-sm" onclick="handleDelete({{ $tag->id }}, '{{ $tag->name }}')">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
                    {{ $tags->appends(['search' => request()->query('search')])->links() }}
                </div>
            @else
                <h3 class="text-center">No Tags yet</h3>
        @endif

        <!-- Modal -->
            <div class="modal fade" id="delete-tag-modal" tabindex="-1" role="dialog" aria-labelledby="delete-tag-modal-label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form id="delete-tag-form" action="" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-tag-modal-title"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-center text-bold">
                                    Are you sure you want to delete this tag?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Confirm</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function handleDelete(id, name) {
            let form = document.querySelector('#delete-tag-form');
            let title = document.querySelector('#delete-tag-modal-title');

            title.textContent = `Delete Tag: ${name}`;

            form.action = `tags/${id}`;
            $('#delete-tag-modal').modal('show');
        }
    </script>

@endsection