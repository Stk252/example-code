@extends('layouts.app')

@section('content')

    <div class="card card-default">
        <h3 class="card-header">
            {{ isset($tag) ? 'Edit Tag' : 'Create Tag' }}
        </h3>
        <div class="card-body">
            @include('partials.errors')
            <form id="tag-form" action="{{ isset($tag) ? route('tags.update', $tag->id) : route('tags.store') }}" method="POST">
                @csrf
                @if (isset($tag))
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ isset($tag) ? $tag->name : '' }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">{{ isset($tag) ? 'Update Tag' : 'Add Tag' }}</button>
                    <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection
