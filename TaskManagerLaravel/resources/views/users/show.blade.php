@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h1>{{ $user->name }}</h1>
            <div>
                @if (auth()->user()->isAdmin() || auth()->user()->id === $user->id)
                    <a href="{{ route('users.edit', $user) }}" class="btn btn-success">Edit</a>
                @endif
                @if (auth()->user()->isAdmin() || auth()->user()->isManager())
                    <a href="{{ route('users.index') }}" class="btn btn-primary">Back to user index</a>
                @endif
            </div>
        </div>

        <div class="card-body">
            @include('partials.errors')
            <form action="" method="POST">
                @csrf
                @method('PUT')

                <div class="d-flex justify-content-center mb-5">
                    <img src="{{ Gravatar::src($user->email) }}" alt="">
                </div>
                <div class="d-flex justify-content-around text-center">
                    <div>
                        <h3>Name</h3>
                        <p>{{ $user->name }}</p>
                    </div>
                    <div>
                        <h3>Role</h3>
                        <p>{{ $user->role }}</p>
                    </div>
                    <div>
                        <h3>Email</h3>
                        <p>{{ $user->email }}</p>
                    </div>
                </div>
                <hr>
                <div class="p-5">
                    <h3>About Me</h3>
                    <p>{{ $user->about }}</p>
                </div>
                <br>
                <hr>
                <div class="d-flex justify-content-around text-center">
                    <div>
                        <h3>Receives task emails</h3>
                        <p>{{ $user->receive_emails ? 'Yes' : 'No' }}</p>
                    </div>
                    <div>
                        <h3>Receives notifications</h3>
                        <p>{{ $user->receive_notifications ? 'Yes' : 'No' }}</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
