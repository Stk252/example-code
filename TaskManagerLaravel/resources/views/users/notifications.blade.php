@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <div>Notifications</div>
            <a href="{{ URL::previous() }}" class="btn btn-info" style="color: white; background: steelblue;">Back</a>
        </div>
        <div class="card-body">
            <ul class="list-group">
                @foreach ($notifications as $notification)
                    <li class="list-group-item d-flex justify-content-between">
                        @if($notification->type == 'App\Notifications\DatabaseNotification')
                            <div>
                                <p>There's new information concerning one of your tasks.</p>
                                <strong>Task: {{ $notification->data['task']['name'] }}</strong>
                            </div>
                            <div>
                                <a href="{{ route('tasks.show', $notification->data['task']['id']) }}" class="btn btn-info btn-sm" style="color: white; background: steelblue;">
                                    View Task
                                </a>
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
