@extends('layouts.app')


@section('content')
    <div class="card card-default">
        <h3 class="card-header">Users</h3>
        <div class="card-body">
            @if ($users->count() > 0)
            <h3>Search for a user</h3>
            <sup>Name, Email or Role</sup>
            <form class="input-group" action="{{ route('users.index') }}" method="GET">
                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->query('search') }}">
            </form>
            <hr>
                <div class="d-flex justify-content-center">
                    {{ $users->appends(['search' => request()->query('search')])->links() }}
                </div>
                <table class="table table-striped table-bordered">

                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <img width="40px" height="40px" src="{{ Gravatar::src($user->email) }}" alt="">
                            </td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role }}</td>

                            <td>
                                <a href="{{ route('users.show', $user->id) }}" class="btn btn-primary btn-sm">View Profile</a>
                                @if(auth()->user()->isAdmin())
                                    <button class="btn btn-danger btn-sm" onclick="handleDelete({{ $user->id }}, '{{ $user->name }}')">Delete</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                <div class="d-flex justify-content-center">
                    {{ $users->appends(['search' => request()->query('search')])->links() }}
                </div>
            @else
                <h3 class="text-center">No users yet</h3>
        @endif
        <!-- Modal -->
            <div class="modal fade" id="delete-user-modal" tabindex="-1" role="dialog" aria-labelledby="delete-user-modal-label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form id="delete-user-form" action="" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-user-modal-title"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-center text-bold">
                                    Are you sure you want to delete this user?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Confirm</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

    <script>
        function handleDelete(id, name) {
            let form = document.querySelector('#delete-user-form');
            let title = document.querySelector('#delete-user-modal-title');

            title.textContent = `Delete User: ${name}`;

            form.action = `users/${id}`;
            $('#delete-user-modal').modal('show');
        }
    </script>

@endsection