@extends('layouts.app')

@section('content')
    <div class="card">
        <h3 class="card-header">{{ $user->name }}</h3>

        <div class="card-body">
            @include('partials.errors')
            <form action="{{ route('users.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}">
                </div>

                @if(auth()->user()->isAdmin())
                <div class="form-group">
                    <label for="role">Role</label>
                    <select id="role" class="form-control" name="role">
                        <option @if ($user->role == 'ADMIN') selected @endif>ADMIN</option>
                        <option @if ($user->role == 'MANAGER') selected @endif>MANAGER</option>
                        <option @if ($user->role == 'DEV') selected @endif>DEV</option>
                    </select>
                </div>
                @endif

                <div class="form-group">
                    <label for="about">About {{ $user->name }}</label>
                    <textarea name="about" id="about" cols="5" rows="5" class="form-control">{{ $user->about }}</textarea>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="receive_emails" id="receive_emails" @if ($user->receive_emails) checked @endif>
                        <label class="form-check-label" for="receive_emails">
                            Receive email notifications
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="receive_notifications" id="receive_notifications" @if ($user->receive_notifications) checked @endif>
                        <label class="form-check-label" for="receive_notifications">
                            Receive task notifications
                        </label>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Update</button>
                <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
            </form>
        </div>
    </div>
@endsection
