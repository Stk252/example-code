<?php

namespace App\Http\Controllers;

use App\Entity\Project;
use App\Http\Requests\Projects\CreateProjectRequest;
use App\Http\Requests\Projects\UpdateProjectRequest;
use App\User;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');
        if ($search) {
            $projects = Project::where('name', 'LIKE', "%{$search}%")
                ->orWhere('status', 'LIKE', "%{$search}%")
                ->paginate(10);
        } else {
            $projects = Project::paginate(10);
        }
        return view('projects.index')->with('projects', $projects);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create')->with('users', User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProjectRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProjectRequest $request)
    {
        $project = Project::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'manager' => $request->manager == 0 ? auth()->user()->id : $request->manager,
            'estimated_project_end' => $request->estimated_project_end == null ? null : \DateTime::createFromFormat('d/m/Y', $request->estimated_project_end)
        ]);

        // Attach the selected users for the many to many relationship
        if ($request->users) {
            $project->users()->attach($request->users);
        }

        session()->flash('success', 'Project created successfully.');

        return redirect(route('projects.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view('projects.show')->with('project', $project)->with('users', User::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        if (!(auth()->user()->isAdmin() || auth()->user()->id == $project->manager)) {
            session()->flash('error', 'You are not authorized to edit this project');
            return redirect()->back();
        }
        return view('projects.create')->with('project', $project)->with('users', User::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProjectRequest $request
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        if (!(auth()->user()->isAdmin() || auth()->user()->id == $project->manager)) {
            session()->flash('error', 'You are not authorized to edit this project');
            return redirect()->back();
        }

        if ($request->users) {
            $project->users()->sync($request->users);
        }

        $project->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'manager' => $request->manager == 0 ? null : $request->manager,
            'estimated_project_end' => $request->estimated_project_end == null ? null : \DateTime::createFromFormat('d/m/Y', $request->estimated_project_end)
        ]);

        session()->flash('success', 'Project updated successfully.');

        return redirect(route('projects.show', $project));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        if (!(auth()->user()->isAdmin() || auth()->user()->id == $project->manager)) {
            session()->flash('error', 'You are not authorized to delete this project');
            return redirect()->back();
        }

        $project->delete();

        session()->flash('success', 'Project deleted successfully.');

        return redirect(route('projects.index'));
    }
}
