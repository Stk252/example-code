<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index() {
        $search = request()->query('search');
        if ($search) {
            $users = User::where('name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%")
                ->orWhere('role', 'LIKE', "%{$search}%")
                ->paginate(10);
        } else {
            $users = User::paginate(10);
        }

        return view('users.index')->with('users', $users);
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (!(auth()->user()->id == $user->id || auth()->user()->isAdmin() || auth()->user()->isManager())) {
            session()->flash('error', 'You are not authorized to view that profile.');
            return redirect()->back();
        }
        return view('users.show')->with('user', $user);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (!(auth()->user()->id == $user->id || auth()->user()->isAdmin())) {
            session()->flash('error', 'You are not authorized to modify that profile.');
            return redirect()->back();
        }
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUserRequest $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role == null ? $user->role : $request->role,
            'receive_emails' => $request->has('receive_emails') ? true : false,
            'receive_notifications' => $request->has('receive_notifications') ? true : false,
            'about' =>$request->about
        ]);

        if (auth()->user()->id == $user->id) {
            session()->flash('success', 'Profile updated successfully');
        } else {
            session()->flash('success', 'User updated successfully');
        }

        return redirect(route('users.show', $user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (auth()->user() === $user) {
            Auth::logout();

            if ($user->delete()) {
                return Redirect::route('login')->with('global', 'Your account has been deleted!');
            }
        }

        $user->delete();

        session()->flash('success', 'User deleted successfully.');

        return redirect(route('users.index'));
    }

    public function notifications() {
        // mark all as read
        auth()->user()->unreadNotifications->markAsRead();

        //display all notifications
        return view('users.notifications', [
            'notifications' => auth()->user()->notifications()->paginate(5)
        ]);
    }
}
