<?php

namespace App\Http\Controllers;

use App\Entity\Project;
use App\Entity\Tag;
use App\Entity\Task;
use App\Http\Requests\Tasks\CreateTaskRequest;
use App\Http\Requests\Tasks\UpdateTaskRequest;
use App\Notifications\DatabaseNotification;
use App\Notifications\EmailNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class TaskController extends Controller
{
    public function __construct()
    {
       $this->middleware('verifyProjectsCount')->only('create', 'store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->query('search');
        if ($search) {
            $tasks = Task::where('name', 'LIKE', "%{$search}%")
                ->orWhere('status', 'LIKE', "%{$search}%")
                ->orWhere('type', 'LIKE', "%{$search}%")
                ->orWhere('severity', 'LIKE', "%{$search}%")
                ->orWhere('priority', 'LIKE', "%{$search}%")
                ->paginate(10);
        } else {
            $tasks = Task::paginate(10);
        }

        return view('tasks.index')->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create')->with('projects', Project::all())->with('tags', Tag::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        // Uploading files in the laravel storage
//        $image = $request->files->store('posts');

        // Create Task
        $task = Task::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'completion_percentage' => $request->completion_percentage,
            'type' => $request->type,
            'severity' => $request->severity,
            'priority' => $request->priority,
            'project_id' => $request->project,
            'deadline' => $request->deadline == null ? null : \DateTime::createFromFormat('d/m/Y', $request->deadline)
        ]);

        // Attach the selected tags for many to many relationship
        if ($request->tags) {
            $task->tags()->attach($request->tags);
        }

        // Attach the selected users for many to many relationship
        if ($request->users) {
            $task->users()->attach($request->users);

            // Need to make this check due to limitations on mails per second sent on mailtrap.io for free users
            // and avoid an application crash

            if (sizeof($task->users) <= 2) {
                foreach ($task->users as $user) {
                    if ($user->receive_emails) {
                        $user->notify(new EmailNotification($task));
                    }
                }
            }

            foreach ($task->users as $user) {
                if ($user->receive_notifications) {
                    $user->notify(new DatabaseNotification($task));
                }
            }
        }

        // flash message
        session()->flash('success', 'Task created successfully.');

        if (URL::previous() == 'tasks') {
            return redirect(route('tasks.index'));
        }
        return redirect('projects/'. $task->project_id);
        // redirect user
    }

    /**
     * Display the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('tasks.show')->with('task', $task)->with('users', User::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('tasks.create')->with('task', $task)->with('projects', Project::all())->with('tags', Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTaskRequest $request
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        if ($request->tags) {
            $task->tags()->sync($request->tags);
        }

        if ($request->users) {
            $task->users()->sync($request->users);

            // Need to make this check due to limitations on mails per second sent on mailtrap.io for free users
            // and avoid an application crash

            if (sizeof($task->users) <= 2) {
                foreach ($task->users as $user) {
                    if ($user->receive_emails) {
                        $user->notify(new EmailNotification($task));
                    }
                }
            }

            foreach ($task->users as $user) {
                if ($user->receive_notifications) {
                    $user->notify(new DatabaseNotification($task));
                }
            }
        }

        $task->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'completion_percentage' => $request->completion_percentage,
            'type' => $request->type,
            'severity' => $request->severity,
            'priority' => $request->priority,
            'deadline' => $request->deadline == null ? null : \DateTime::createFromFormat('d/m/Y', $request->deadline)
        ]);

        session()->flash('success', 'Task updated successfully.');

        return redirect(route('tasks.show', $task));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        // Deleting the eventual files
//        Storage::delete($task->files);
        $task->delete();

        session()->flash('success', 'Task deleted successfully.');

        if (URL::previous() == route('tasks.index')) {
            return redirect(route('tasks.index'));
        }

        return redirect('projects/'. $task->project_id);

    }
}
