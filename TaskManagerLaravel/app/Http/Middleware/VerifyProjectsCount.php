<?php

namespace App\Http\Middleware;

use App\Entity\Project;
use Closure;

class VerifyProjectsCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Project::all()->count() === 0) {
            session()->flash('error', 'There are no projects to add tasks to at the moment.');
            return redirect(route('projects.create'));
        }
        return $next($request);
    }
}
