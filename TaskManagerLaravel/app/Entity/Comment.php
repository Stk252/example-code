<?php

namespace App\Entity;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'content',
        'user_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
