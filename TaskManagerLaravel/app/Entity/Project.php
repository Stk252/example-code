<?php

namespace App\Entity;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name',
        'description',
        'status',
        'estimated_project_end',
        'manager'
    ];

    public function manager()
    {
        return $this->belongsTo(User::class)->where('role' == 'MANAGER' or 'role' == 'ADMIN');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Checks if a task has a specific tag
     *
     * @param int $userId
     * @return bool
     */
    public function hasUser($userId)
    {
        return in_array($userId, $this->users->pluck('id')->toArray());
    }
}
