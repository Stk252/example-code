<?php

namespace App\Entity;

use App\User;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
//    use softDeletes;

    protected $fillable = [
        'name',
        'description',
        'status',
        'completion_percentage',
        'type',
        'severity',
        'priority',
        'deadline',
        'project_id'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Checks if a task has a specific tag
     *
     * @param int $tagId
     * @return bool
     */
    public function hasTag($tagId)
    {
        return in_array($tagId, $this->tags->pluck('id')->toArray());
    }

    /**
     * Checks if a task has a specific tag
     *
     * @param int $userId
     * @return bool
     */
    public function hasUser($userId)
    {
        return in_array($userId, $this->users->pluck('id')->toArray());
    }
}
