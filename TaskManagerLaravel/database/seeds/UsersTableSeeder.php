<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $santiago = User::where('email', 'santiago@taskmanager.com')->first();

        if (!$santiago) {
            User::create([
                'name' => 'Santiago Kummert',
                'email' => 'santiago@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'ADMIN'
            ]);
        }

        $gg = User::where('email', 'gg@taskmanager.com')->first();

        if (!$gg) {
            User::create([
                'name' => 'Gérard Ernaelsten',
                'email' => 'gg@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'ADMIN'
            ]);
        }

        $walt = User::where('email', 'walt@taskmanager.com')->first();

        if (!$walt) {
            User::create([
                'name' => 'Walt Disney',
                'email' => 'walt@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'ADMIN'
            ]);
        }

        $mickey = User::where('email', 'mickey@taskmanager.com')->first();

        if (!$mickey) {
            User::create([
                'name' => 'Mickey Mouse',
                'email' => 'mickey@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'MANAGER'
            ]);
        }

        $minnie = User::where('email', 'minnie@taskmanager.com')->first();

        if (!$minnie) {
            User::create([
                'name' => 'Minnie Mouse',
                'email' => 'minnie@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'MANAGER'
            ]);
        }

        $donald = User::where('email', 'donald@taskmanager.com')->first();

        if (!$donald) {
            User::create([
                'name' => 'Donald Duck',
                'email' => 'donald@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'MANAGER'
            ]);
        }

        $goofy = User::where('email', 'goofy@taskmanager.com')->first();

        if (!$goofy) {
            User::create([
                'name' => 'Goofy Goof',
                'email' => 'goofy@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'MANAGER'
            ]);
        }

        $max = User::where('email', 'max@taskmanager.com')->first();

        if (!$max) {
            User::create([
                'name' => 'Max Goof',
                'email' => 'max@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $scrooge = User::where('email', 'scrooge@taskmanager.com')->first();

        if (!$scrooge) {
            User::create([
                'name' => 'Scrooge McDuck',
                'email' => 'scrooge@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $pete = User::where('email', 'pete@taskmanager.com')->first();

        if (!$pete) {
            User::create([
                'name' => 'Peter Pete Sr.',
                'email' => 'pete@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $huey = User::where('email', 'huey@taskmanager.com')->first();

        if (!$huey) {
            User::create([
                'name' => 'Huey Duck',
                'email' => 'huey@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $dewey = User::where('email', 'dewey@taskmanager.com')->first();

        if (!$dewey) {
            User::create([
                'name' => 'Dewey Duck',
                'email' => 'dewey@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $louie = User::where('email', 'louie@taskmanager.com')->first();

        if (!$louie) {
            User::create([
                'name' => 'Louie Duck',
                'email' => 'louie@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $clarabelle = User::where('email', 'clarabelle@taskmanager.com')->first();

        if (!$clarabelle) {
            User::create([
                'name' => 'Clarabelle Cow',
                'email' => 'clarabelle@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $mortimer = User::where('email', 'mortimer@taskmanager.com')->first();

        if (!$mortimer) {
            User::create([
                'name' => 'Mortimer Mouse',
                'email' => 'mortimer@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $simba = User::where('email', 'simba@taskmanager.com')->first();

        if (!$simba) {
            User::create([
                'name' => 'Simba',
                'email' => 'simba@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $mufasa = User::where('email', 'mufasa@taskmanager.com')->first();

        if (!$mufasa) {
            User::create([
                'name' => 'Mufasa',
                'email' => 'mufasa@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $scar = User::where('email', 'scar@taskmanager.com')->first();

        if (!$scar) {
            User::create([
                'name' => 'Scar',
                'email' => 'scar@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $pumba = User::where('email', 'pumba@taskmanager.com')->first();

        if (!$pumba) {
            User::create([
                'name' => 'Pumba',
                'email' => 'pumba@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $timon = User::where('email', 'timon@taskmanager.com')->first();

        if (!$timon) {
            User::create([
                'name' => 'Timon',
                'email' => 'timon@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $nala = User::where('email', 'nala@taskmanager.com')->first();

        if (!$nala) {
            User::create([
                'name' => 'Nala',
                'email' => 'nala@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $zazu = User::where('email', 'zazu@taskmanager.com')->first();

        if (!$zazu) {
            User::create([
                'name' => 'Zazu',
                'email' => 'zazu@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $rafiki = User::where('email', 'rafiki@taskmanager.com')->first();

        if (!$rafiki) {
            User::create([
                'name' => 'Rafiki',
                'email' => 'rafiki@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $jaq = User::where('email', 'jaq@taskmanager.com')->first();

        if (!$jaq) {
            User::create([
                'name' => 'Jaq',
                'email' => 'jaq@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $gus = User::where('email', 'gus@taskmanager.com')->first();

        if (!$gus) {
            User::create([
                'name' => 'Gus',
                'email' => 'gus@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $lucifer = User::where('email', 'lucifer@taskmanager.com')->first();

        if (!$lucifer) {
            User::create([
                'name' => 'Lucifer',
                'email' => 'lucifer@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $pooh = User::where('email', 'pooh@taskmanager.com')->first();

        if (!$pooh) {
            User::create([
                'name' => 'Winnie the Pooh',
                'email' => 'pooh@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $tigger = User::where('email', 'tigger@taskmanager.com')->first();

        if (!$tigger) {
            User::create([
                'name' => 'Tigger',
                'email' => 'tigger@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $piglet = User::where('email', 'piglet@taskmanager.com')->first();

        if (!$piglet) {
            User::create([
                'name' => 'Piglet',
                'email' => 'piglet@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $kanga = User::where('email', 'kanga@taskmanager.com')->first();

        if (!$kanga) {
            User::create([
                'name' => 'Kanga',
                'email' => 'kanga@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $roo = User::where('email', 'roo@taskmanager.com')->first();

        if (!$roo) {
            User::create([
                'name' => 'Roo',
                'email' => 'roo@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $eeyore = User::where('email', 'eeyore@taskmanager.com')->first();

        if (!$eeyore) {
            User::create([
                'name' => 'Eeyore',
                'email' => 'eeyore@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $lumiere = User::where('email', 'lumiere@taskmanager.com')->first();

        if (!$lumiere) {
            User::create([
                'name' => 'Lumière',
                'email' => 'lumiere@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $cogsworth = User::where('email', 'cogsworth@taskmanager.com')->first();

        if (!$cogsworth) {
            User::create([
                'name' => 'Cogsworth',
                'email' => 'cogsworth@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $potts = User::where('email', 'potts@taskmanager.com')->first();

        if (!$potts) {
            User::create([
                'name' => 'Mrs. Potts',
                'email' => 'potts@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $chip = User::where('email', 'chip@taskmanager.com')->first();

        if (!$chip) {
            User::create([
                'name' => 'Chip Potts',
                'email' => 'chip@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $jiminy = User::where('email', 'jiminy@taskmanager.com')->first();

        if (!$jiminy) {
            User::create([
                'name' => 'Jiminy Cricket',
                'email' => 'jiminy@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $pinocchio = User::where('email', 'pinocchio@taskmanager.com')->first();

        if (!$pinocchio) {
            User::create([
                'name' => 'Pinocchio',
                'email' => 'pinocchio@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $geppetto = User::where('email', 'geppetto@taskmanager.com')->first();

        if (!$geppetto) {
            User::create([
                'name' => 'Geppetto',
                'email' => 'geppetto@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }

        $figaro = User::where('email', 'figaro@taskmanager.com')->first();

        if (!$figaro) {
            User::create([
                'name' => 'Figaro',
                'email' => 'figaro@taskmanager.com',
                'password' => Hash::make('password'),
                'role' => 'DEV'
            ]);
        }


    }
}
