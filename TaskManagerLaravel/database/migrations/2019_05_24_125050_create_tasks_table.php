<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('name');
            $table->text('description');
            $table->enum('status', ['On Hold', 'In Progress', 'Finished', 'Closed'])->default('On Hold');
            $table->integer('completion_percentage');
            $table->enum('type', ['Bug', 'Feature'])->default('Bug');
            $table->enum('severity', ['Very Low', 'Low', 'Medium', 'High', 'Very High', 'Critical'])->default('Medium');
            $table->enum('priority', ['Very Low', 'Low', 'Medium', 'High', 'Very High'])->default('Medium');
            $table->dateTime('deadline')->nullable();
            $table->timestamps();
            $table->unique(array('name', 'project_id'), 'unique_task_per_project');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
