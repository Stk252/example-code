# Example Code

Voici quelques exemples de code :

## BooksQL

Ce dossier contient deux applications
- Backend en Laravel
- Frontend en VueJS (avec Tailwind CSS)
- Base de données en PostgreSQL

BooksQL est une application très simple qui affiche un catalogue de livres

C'était un premier essai pour apprendre a utiliser GraphQL avec Laravel via 
Lighthouse et VueJS via Apollo
Elle est basée sur un tutoriel que j'ai suivi sur YouTube

## JavaScript

Ce dossier contient quelques petites applications que j'ai développées lorsque
j'apprennais JavaScript

#### Github Finder 
on peut rechercher 1 utilisateur sur Github et voir quelques informations sur 
l'utilisateur et des liens ver ses derniers repositories

#### Html Video Player 
1 lecteur vidéo en HTML et JavaScript très simplifié (date d'il y a longtemps)

#### Loan Calculator
Permet de rentrer 1 somme d'argent, un taux d'intérêt et un nombre d'années,
retourne les paiements mensuels, le paiement total et le total d'intérêt à payer

#### Task List
1 TODO list très simple avec persitence dans le localStorage

#### Tracalorie Project
Permet d'insérer des aliments avec leur taux de calories et en fait la somme
Application faite surtout pour apprendre le Module Pattern de JavaScript.

Il ce peut que certaines fonctionnalités ne marchent pas sur tous les browsers.


## TaskManagerLaravel

Application Laravel pour la gestion de taches et leur suivi, j'ai du la 
développer pour mon examen de PHP de 3è année.

Application pas totalement terminée car développée en 5 jours quand je me suis 
rendu compte que je n'aurais pas le temps de développer le Front End en Vue pour
l'application que j'essaiyais de faire en Symfony 4 API Platform.

- Bootstrap pour le style
- Gestion des utilisateurs, 3 rôles : admin, manager et dev
- Création et suivi de Projets
- Création et suivi de tâches dans chaque projet
- Base de données en MariaDB, tables générées par des migrations
- Fichier TaskManagerLaravelEA.png montre le diagramme ER
- Intégration de modules JavaScript : Select2, air-datepicker et tinymce
- Envoi de mails et/ou de notifications lors de la création et modification de taches pour les utilisateurs impliqués
- Pas de gestion des commentaires car manque de temps (même si la table est là)


## TaskManager_Backend

Application initiale pour mon examen PHP, j'ai travaillé plusieurs semaines
dessus en apprenant Symfony 4 API Platform, l'API marche pour la plupart,
Il y a quelques endroits où ça ne marche pas comme je m'y attendais, 
j'ai encore des lacunes notamment avec les annotations @ApiResource et leur 
comportement.

Base de données en MariaDB, tables crées par migrations

Application abandonnée quand j'ai remarqué que je n'aurais pas le temps pour
faire le FrontEnd en Vue (Que je comprends suite à quelques cours en ligne mais que je ne maitrise pas encore)

## Vue
Trois petits projets suivant mon cours en ligne de VueJS

Ce sont des exercices dont les consignes sont données dans le cours, 
La corrections sont aussi données, je les ai regardées une fois mes propres
essais terminés et j'y ai appliqué quelques corrections.

- La première n'est pas une application Vue mais Vue est appliqué via CDN
- La deuxième est très basique et était un exercice d'utilisation de components
- La troisième est plus complète, contient des directives, du routage, Vue resource et Vuex

J'ai aussi suivi un cours Nuxt à la suite de celà mais je n'ai pas vraiment
d'application de mise en pratique. L'application développé est surtout 
développée par l'instructeur et pas vraiment par moi.

Si bien je comprends Vue de manière générale je n'ai pas encore de vraie maitrise.


## Travaux d'analyse
Ce sont des grands rapports qu'il ne faut pas lire mais que j'ai mis au cas où,
contiennent surtout des éléments liés à l'analyse.

#### W-SHARE - Rapport Final

Projet réalisé pour mon examen de C# en WPF.
C'a n'a rien à voir avec le PHP mais il y a des éléments d'analyse, 
des diagrammes, des captures de sketchs réalisés sur Adobe XD, des captures de
l'application finales.

#### Travail PRAC

Rapport de mon travail pour le cours 'Projet d'Analyse et de Conception',
et qui était à la base pour mon travail de fin d'études (qui est en arrêt 
pour le moment par problèmes de santé de mon promoteur).



